package com.vittlepix
import scala.collection.JavaConversions._
import org.holoeverywhere.app.Activity
import org.holoeverywhere.widget.TextView
import org.holoeverywhere.ArrayAdapter
import com.actionbarsherlock.view.Menu
import com.actionbarsherlock.view.MenuItem
import com.boosed.vp.shared.db.embed._
import com.boosed.vp.shared.db._
import com.handmark.pulltorefresh.library.PullToRefreshListView
import com.vittlepix.traits._
import com.vittlepix.widget._
import android.os.Bundle
import android.os.Message
import android.view.View
import java.util.List
import java.util.Arrays

class MainActivity extends Activity
  with Preparable
  with Asyncable
  with Handlable
  with Imageable
  with Loggable
  with Navigable
  with Rpc
  with Restorable
  with Shortcuts
  with Toastable
  with Viewable {

  override def onCreate(state: Bundle) {
    super.onCreate(state)
    setContentView(R.layout.main)

    Option(state) match {
      case Some(x) => // do nothing

      // init cuisines
      case None =>
        async[Unit, Unit, List[Cuisine]] { (_, _) =>
          service.loadCuisines
        } then {
          cuisines = _
        } otherwise { e =>
          error("could not load cuisines", e)
        } execute ()
    }
  }

  override def onSaveInstanceState(state: Bundle) {
    super.onSaveInstanceState(state)
    implicit def store = state

    toState(MainActivity.stateCuisines, cuisines) // cuisines
    toState(MainActivity.stateKitchens, adapterKitchen.items) // kitchens
  }

  override def onRestoreInstanceState(state: Bundle) {
    super.onRestoreInstanceState(state)

    fromState(state, MainActivity.stateCuisines)(cuisines = _: List[Cuisine]) // cuisines
    fromState(state, MainActivity.stateKitchens)(adapterKitchen.addData(_: List[Kitchen])) // kitchens
  }

  override def onCreateOptionsMenu(menu: Menu) = {
    getSupportMenuInflater.inflate(R.menu.main, menu)
    true
  }

  override def onMenuItemSelected(feautreId: Int, item: MenuItem) = item.getItemId match {
    // home
    case android.R.id.home =>
      toastShort("you hit the home button see")
      true

    // search/filter kitchens
    case R.id.search =>
      async[Unit, Unit, List[Kitchen]] { (_, _) =>
        service.search(Longs(), "", Doubles(), 0, 25)
      } inform {
        R.string.app_name
      } then { result =>
        // set tags on kitchens
        adapterKitchen setData addCuisines(result)
      } execute ()

      actionBar.setSelectedNavigationItem(0)
      true

    // add new kitchens, ensure logged in
    case R.id.add =>
      //toastShort("you want to add a new kitchen")
      val kitchen = new Kitchen
      val address = new Address
      address.street1 = "123 Sesame Street"
      address.street2 = "Ste 13432"
      address.city = "New York"
      address.statoid = "NY"
      address.postal = "10940"

      kitchen.address = address
      kitchen.name = "Example Kitchen"
      kitchen.desc = "This is an example of a kitchen how it appears to the user in context of a listview so you shouldn't " +
        "put much stock into what's happening because it isn't a good example I don't know why this isn't much longer"
      kitchen.phone = "(123)456-7890"

      //adapterKitchen.addData(List(kitchen))
      true
    case _ => true
  }

  // navigation handler
  override def onNavigationItemSelected(position: Int, id: Long) = position match {
    // search
    //case 0 =>
    //  toastShort("search")
    //filter.setVisible(false)
    //  true

    // latest
    case 1 =>
      toastShort("latest additions")
      // filter.setVisible(true)
      true

    // favorites
    case 2 =>
      toastShort("these are favorites")
      //filter.setVisible(false)
      true

    // managed
    case 3 =>
      toastShort("must be logged in to manage your kitchens")
      // filter.setVisible(false)
      true
    case _ => true
  }

  override def handleMessage(msg: Message) = msg.what match {
    case _ => true
  }

  // update actionbar, etc.
  override def prepare {
    super.prepare

    // init action bar
    actionBar.setDisplayShowTitleEnabled(false)
    actionBar.setHomeButtonEnabled(true)

    // prepare list
    //list.setDividerPadding(1)
    //list.setAdapter(adapter)
    list.setAdapter(adapterKitchen)
    list.setPullLabel(string(R.string.ptr_pull))
    list.setReleaseLabel(string(R.string.ptr_release))
    list.setRefreshingLabel(string(R.string.ptr_refresh))
    list.setEmptyView(empty)

    //    // refresh listener
    //    list.setOnRefreshListener(new OnRefreshListener[android.widget.ListView] {
    //      override def onRefresh(view: PullToRefreshBase[android.widget.ListView]) {
    //        view.setLastUpdatedLabel(adapterKitchen.getCount() + " items shown")
    //
    //        // work to refresh list
    //        async[String, Unit, List[String]] { (_, params) =>
    //          Thread.sleep(2000)
    //          params.toList
    //        } then { result =>
    //          adapterKitchen.addData(result)
    //          adapterKitchen.notifyDataSetChanged
    //          //mListItems.addFirst("Added after refresh...");
    //
    //          // Call onRefreshComplete when the list has been refreshed.
    //          view.onRefreshComplete
    //
    //          //          handler.postDelayed(new Runnable {
    //          //            override def run = view.setPullToRefreshEnabled(false)
    //          //          }, 500)
    //          //async[Unit, Unit, Unit] { (_, _) => } post { () => view.setPullToRefreshEnabled(false) } execute()
    //          //view.setPullToRefreshEnabled(false)
    //          //refreshView.setShowIndicator(false)
    //        } execute ("johnny")
    //      }
    //    })

    //    // Add an end-of-list listener
    //    list.setOnLastItemVisibleListener(new OnLastItemVisibleListener {
    //      override def onLastItemVisible {
    //        toastShort("should we disable this: %s" format loading)
    //      }
    //    })

    //val actualListView = list.getRefreshableView

    /**
     * sound event listener
     */
    //SoundPullEventListener<ListView> soundListener = new SoundPullEventListener<ListView>(this);
    //soundListener.addSoundEvent(State.PULL_TO_REFRESH, R.raw.pull_event);
    //soundListener.addSoundEvent(State.RESET, R.raw.reset_sound);
    //soundListener.addSoundEvent(State.REFRESHING, R.raw.refreshing_sound);
    //mPullRefreshListView.setOnPullEventListener(soundListener);

    // You can also just use setListAdapter(mAdapter) or
    // mPullRefreshListView.setAdapter(mAdapter)
    //actualListView.setAdapter(adapter)

  }

  // case constructor for vararg Long to java.util.List[java.lang.Long], uses ascription
  case object Longs {
    def apply(xs: Long*) = Arrays.asList(xs.map(a => a: java.lang.Long): _*)
  }

  case object Doubles {
    def apply(xs: Double*) = Arrays.asList(xs.map(a => a: java.lang.Double): _*)
  }

  def onControl(v: View) = v.getId match {
    case _ => // do nothing, not a recognized widget
  }

  /**
   * add cuisine names as kitchen tags field
   */
  private def addCuisines(kitchens: List[Kitchen]) = {
    // append cuisines
    for (
      k <- kitchens;
      kc <- k.cuisines;
      c <- cuisines if (c.getId == kc.getId)
    ) k.tags = k.tags concat ", " concat c.name

    // drop leading separator
    kitchens foreach (k => k.tags = k.tags.drop(2))

    kitchens
  }

  // navigation adapter
  override lazy val adapterNavigation = new ArrayAdapter[String](this,
    org.holoeverywhere.R.layout.sherlock_spinner_item, getResources.getStringArray(R.array.search))

  // kitchen adapter
  lazy val adapterKitchen = new BaseArrayAdapter[Kitchen, RowKitchen](
    this,
    R.layout.row_kitchen,
    new RowKitchen(_),
    (row, item) => {
      item.image.url match {
        case "" => row.showPhoto(false);
        case url =>
          row.showPhoto(true)
          display(url, row.image, true)
      }

      row.title.setText(item.name)
      row.location.setText(item.toString.replaceAll("<br/>", "\n"))
      row.content.setText(item.desc)
      row.getCategory.setText(item.tags)
    })

  // widgets
  lazy val list: PullToRefreshListView = R.id.list_kitchens
  lazy val empty: TextView = android.R.id.empty

  // state
  var cuisines: List[Cuisine] = _
}

object MainActivity {

  // states
  val stateCuisines = "com.vittlepix.main.state.Cuisines"
  val stateKitchens = "com.vittlepix.main.state.Kitchens"
}