package com.vittlepix

import org.holoeverywhere.app.Activity
import org.holoeverywhere.widget.Spinner
import org.holoeverywhere.ArrayAdapter
import com.vittlepix.traits._
import scala.collection.JavaConversions._
import android.os.Bundle
import java.util.Arrays

class Query extends Activity
  with Preparable
  with Viewable {

  override protected def onCreate(state: Bundle) {
    super.onCreate(state)
    setContentView(R.layout.query)
  }

  def prepare {

    val words = Arrays.asList("one", "two", "three")
    val adapter = new ArrayAdapter[String](this, org.holoeverywhere.R.layout.simple_spinner_item, words)
    adapter.setDropDownViewResource(org.holoeverywhere.R.layout.simple_spinner_dropdown_item)
    spinner1.setAdapter(adapter)
  }

  lazy val spinner1: Spinner = R.id.spinner1
  lazy val spinner2: Spinner = R.id.spinner2
  lazy val spinner3: Spinner = R.id.spinner3
}