package com.vittlepix.traits
import org.holoeverywhere.app.Activity
import org.holoeverywhere.ArrayAdapter
import com.actionbarsherlock.app.ActionBar.OnNavigationListener
import com.actionbarsherlock.app.ActionBar
import android.content.DialogInterface
import android.os.Handler
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.View
import android.text.TextUtils

trait Shortcuts { self: Activity =>
  def string = getString _
  def resources = getResources
  def isEmpty = TextUtils.isEmpty _
  lazy val actionBar = getSupportActionBar
}

/**
 * activity that requires handler dispatch
 */
trait Handlable extends Handler.Callback { self: Activity =>

  // implement message handling via callback
  val handler = new Handler(this)
}

/**
 * activity has list navigation enabled
 */
trait Navigable extends Activity
  with Preparable
  with OnNavigationListener {

  val adapterNavigation: ArrayAdapter[String]

  /**
   * prepare navigation array adapter
   */
  override def prepare {
    adapterNavigation.setDropDownViewResource(org.holoeverywhere.R.layout.sherlock_spinner_dropdown_item)
    getSupportActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST)
    getSupportActionBar.setListNavigationCallbacks(adapterNavigation, this)
  }
}

/**
 * initialize the ui wrt activity lifecycle
 */
trait Preparable extends Activity {

  /**
   * prep the ui
   */
  def prepare

  override def setContentView(view: Int) {
    super.setContentView(view)
    prepare
  }

  override def setContentView(view: View) {
    super.setContentView(view)
    prepare
  }
}

/**
 * for activity/widget which needs implicit conversions from int to views, closures to listeners
 */
trait Viewable { self: Activity => //{ def findViewById(id: Int): View } =>

  /**
   * id to view
   */
  implicit def int2View[A <: View](id: Int): A = findViewById(id).asInstanceOf[A]

  //  implicit def int2ViewM[A : Manifest](id: Int): A = manifest[A] match {
  //    //case a if a <:< Manifest.classType(classOf[A]) => findViewById(id).asInstanceOf[A]
  //    case a if a <:< Manifest.classType(classOf[String]) => getString(id).asInstanceOf[A]
  //    case _ => Option("free").orNull.asInstanceOf[A]
  //  }

  /**
   * like above, but search in the provided <code>View</code>
   */
  def view[A <: View](id: Int, view: View): A = view.findViewById(id).asInstanceOf[A]

  /**
   * func to View.OnClickListener
   */
  implicit def func2ClickListener(f: View => Unit): View.OnClickListener = new View.OnClickListener {
    override def onClick(v: View) = f(v)
  }

  /**
   * func to DialogInterface.OnClickListener
   */
  implicit def func2DialogClickListener(f: (DialogInterface, Int) => Unit) = new DialogInterface.OnClickListener {
    override def onClick(dialog: DialogInterface, which: Int) = f(dialog, which)
  }

  /**
   * func to OnMultiChoiceClickListener
   */
  implicit def func2MultiChoiceDialogClickListener(f: (DialogInterface, Int, Boolean) => Unit) =
    new DialogInterface.OnMultiChoiceClickListener {
      override def onClick(dialog: DialogInterface, which: Int, checked: Boolean) = f(dialog, which, checked)
    }

  /**
   * func to OnCancelListener
   */
  implicit def func2OnCancelListener(f: DialogInterface => Unit) = new DialogInterface.OnCancelListener {
    override def onCancel(dialog: DialogInterface) = f(dialog)
  }
}

/**
 * did not use implicit conversions because type restrictions can only be enforced on trait/class "type parameters"
 * (and method params) and leads to ambiguity when traits mixed in conjunction.
 */
trait Animatable { self: Activity =>

  /**
   * id to animation
   */
  def anim(id: Int): Animation = AnimationUtils.loadAnimation(this, id)
}