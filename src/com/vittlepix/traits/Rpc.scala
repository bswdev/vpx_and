package com.vittlepix.traits
import com.gdevelop.gwt.syncrpc.SyncProxy
import com.boosed.vp.shared.rpc.RpcService

/**
 * rpc stuff goes here
 */
trait Rpc {

  lazy val service = Rpc.service match {
    case Some(x) => x

    // initialize service
    case None =>
      Rpc.service = Option(SyncProxy.newProxyInstance(
        classOf[RpcService],
        "http://www.vittlepix.com/mnu/", // module
        "rpc") // endpoint
        )
      Rpc.service.get
  }
}

object Rpc {

  // rpc service
  var service: Option[RpcService] = None
}