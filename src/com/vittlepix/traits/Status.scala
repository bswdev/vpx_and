package com.vittlepix.traits
import org.holoeverywhere.widget.Toast
import android.util.Log
import org.holoeverywhere.app.Dialog
import org.holoeverywhere.app.ProgressDialog
import org.holoeverywhere.app.Activity

/**
 * logging
 */
trait Loggable {
  val tag = "vittlepix"
  def debug(msg: String, t: Throwable = null) = Option(t).toRight(()).fold(_ => Log.d(tag, msg), Log.d(tag, msg, _))
  def error(msg: String, t: Throwable = null) = Option(t).toRight(()).fold(_ => Log.e(tag, msg), Log.e(tag, msg, _))
  def info(msg: String, t: Throwable = null) = Option(t).toRight(()).fold(_ => Log.i(tag, msg), Log.i(tag, msg, _))
  def verbose(msg: String, t: Throwable = null) = Option(t).toRight(()).fold(_ => Log.v(tag, msg), Log.v(tag, msg, _))
  def warn(msg: String, t: Throwable = null) = Option(t).toRight(()).fold(_ => Log.w(tag, msg), Log.w(tag, msg, _))
  def wtf(msg: String, t: Throwable = null) = Option(t).toRight(()).fold(_ => Log.wtf(tag, msg), Log.wtf(tag, msg, _))
}

/**
 * toasting
 */
trait Toastable { self: Activity =>
  def toastShort(msg: Int) = Toast.makeText(getApplicationContext, msg, Toast.LENGTH_SHORT).show
  def toastShort(msg: String) = Toast.makeText(getApplicationContext, msg, Toast.LENGTH_SHORT).show
  def toastLong(msg: Int) = Toast.makeText(getApplicationContext, msg, Toast.LENGTH_LONG).show
  def toastLong(msg: String) = Toast.makeText(getApplicationContext, msg, Toast.LENGTH_LONG).show
}

/**
 * alerting w/ dialog
 */
trait Alertable extends Loggable { self: Activity =>
  var dialog: Option[Dialog] = None

  def alert(msg: Int) = msg match {
    case -1 => // do nothing
    case msg =>
      try
        // create dialog
        dialog = Option(ProgressDialog.show(this, null, getString(msg), true, true))
      catch {
        // invalid message or activity state
        case e =>
          dialog = None
          warn("owning activity stopped", e)
      }
  }

  def endAlert = dialog match {
    case None => // ignored
    case Some(d) =>
      d.dismiss
      dialog = None
  }
}