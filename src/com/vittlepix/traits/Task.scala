package com.vittlepix.traits

import org.holoeverywhere.app.Activity

import android.os.AsyncTask

/**
 * Simplifying async tasks.
 */
trait Asyncable extends Alertable { self: Activity =>

  // seems to be easier to call this explicitly to be able to use function literals
  implicit def async[T, P, R](subject: ((P*) => Unit, T*) => R) = new AsyncBuilder[T, P, R](subject)

  // design a dsl for the above class
  protected class AsyncBuilder[T, P, R](subject: ((P*) => Unit, T*) => R) {

    var message = -1
    var prolog: Option[() => Unit] = None
    var epilog: Option[() => Unit] = None
    var predicate: Option[R => Unit] = None
    var caveat: Option[Throwable => Unit] = None
    var progress: Option[(P*) => Unit] = None
    //var publish: (P*) => Unit = _ 

    // this is a very basic internal DSL
    def apply(ps: T*) = execute(ps: _*)
    // include a dialog message
    def inform(m: Int) = { message = m; this }
    // ui before work
    def pre(p: () => Unit) = { prolog = Option(p); this }
    // during work
    def update(u: (P*) => Unit) = { progress = Option(u); this }
    // ui after work
    def post(p: () => Unit) = { epilog = Option(p); this }
    // if succeeded
    def then(p: R => Unit) = { predicate = Option(p); this }
    // if failed
    def otherwise(c: Throwable => Unit) = { caveat = Option(c); this }

    // can either invoke directly or via apply (hardwired progress to Int)
    def execute(ts: T*): Unit = new AsyncTask[T, P, Throwable Either R] {

      override def onPreExecute {
        super.onPreExecute

        // execute prolog
        prolog match {
          case Some(x) => x()
          case None => // do nothing
        }

        // handle message
        alert(message)
      }

      override def doInBackground(params: T*) = try Right(subject(publishProgress(_: _*), params: _*)) catch { case e => Left(e) }

      override def onProgressUpdate(status: P*) = progress match {
        case Some(x) => x(status: _*)
        case None => // do nothing
      }

      // perform caveat or predicate
      override def onPostExecute(res: Throwable Either R) {
        super.onPostExecute(res)

        // handle message
        endAlert

        epilog match {
          case Some(x) => x()
          case None => // do nothing
        }

        res.fold(
          t => caveat.toLeft(()).fold(_(t), _ => info("throwable, but no caveat", t)),
          r => predicate.toLeft(()).fold(_(r), _ => info("result, but no predicate")))
      }
    } execute (ts: _*)
  }
}