package com.vittlepix.widget


import scala.collection.JavaConversions._
import com.vittlepix.traits.Loggable
import org.holoeverywhere.ArrayAdapter
import org.holoeverywhere.LayoutInflater
import android.view.ViewGroup
import android.content.Context
import android.view.View
import java.util.List
import java.util.ArrayList

/**
 * A is the stored type
 * B is the row wrapper implementation
 */
class BaseArrayAdapter[A, B](
  context: Context,
  listItemResourceId: Int,
  create: View => B, // create the view
  render: (B, A) => Unit, // render the view
  val items: List[A] = new ArrayList[A]
  )
  extends ArrayAdapter[A](context, 0, items)
  with Loggable {

  override def getView(position: Int, view: View, parent: ViewGroup) = {
    val _view = Option(view) match {
      case Some(x) => x
      
      // create view if not present; create and set wrapper
      case None =>
        val x = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE).asInstanceOf[LayoutInflater]
          .inflate(listItemResourceId, parent, false)
        x.setTag(create(x))
        x
    }

    // check for array out of bounds
    (try Right(getItem(position)) catch {
      case e =>
        error("base array adapter out of bounds: %d" format position, e)
        Left(e)
    }) fold (
      // no item, just return view
      _ => _view,
      
      // render item
      item => {
        render(_view.getTag.asInstanceOf[B], item)
        _view
      })
  }

  // add additional data to the backing list
  def addData(items: List[A]) {
    this.items.addAll(items)
    notifyDataSetChanged
  }

  // set the complete backing list data
  def setData(items: List[A]) {
    this.items.clear
    addData(items)
  }
  
  def reset = setData(Nil)
}