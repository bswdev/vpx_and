package com.vittlepix.widget

import com.vittlepix.traits._
import com.vittlepix.R

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View.OnClickListener
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout

/**
 * edit text with a clearable button
 */
class ClearableEditText(context: Context, attrs: AttributeSet)
  extends RelativeLayout(context, attrs) {

  // layout
  context.getSystemService(Context.LAYOUT_INFLATER_SERVICE).asInstanceOf[LayoutInflater]
    .inflate(R.layout.wgt_clear_edit, this, true)

  // styles from the layout xml
  val ta = context.obtainStyledAttributes(attrs, R.styleable.com_vittlepix_widget_ClearableEditText)

  // text style
  text.setTextAppearance(
    context,
    ta.getResourceId(
      R.styleable.com_vittlepix_widget_ClearableEditText_textStyle,
      android.R.style.TextAppearance_Medium))

  // text hint
  text.setHint(ta.getString(R.styleable.com_vittlepix_widget_ClearableEditText_android_hint))

  // don't forget this!
  ta.recycle

  // listeners

  // clear listener
  clear.setOnClickListener(new OnClickListener { override def onClick(v: View) = text.setText("") })

  // toggle listener
  text.addTextChangedListener(new TextWatcher {
    override def beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override def afterTextChanged(editable: Editable) {}
    override def onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) =
      // hide/show button wrt the string
      clear.setVisibility(if (s.length > 0) View.VISIBLE else View.GONE)
  })

  def getText = text getText
  def setText(s: CharSequence) = text setText s
  def setSelection(index: Int) = text setSelection index

  // widgets (not sure why viewable does not work here)
  lazy val text = findViewById(R.id.edit).asInstanceOf[EditText]
  lazy val clear = findViewById(R.id.clear).asInstanceOf[Button]
}