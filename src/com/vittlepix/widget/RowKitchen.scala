package com.vittlepix.widget
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget._
import com.vittlepix.traits.Viewable
import com.vittlepix.R


class RowKitchen(base: View) extends Wrappable {

  override protected val _base  = base
  
  lazy val frame: FrameLayout = R.id.frame
  lazy val image: ImageView = android.R.id.icon
  lazy val title: TextView = android.R.id.text1
  lazy val distance: TextView = R.id.distance
  lazy val location: TextView = R.id.location
  //lazy val time: TextView = R.id.time
  lazy val content: TextView = android.R.id.text2
  //lazy val count: TextView = R.id.count
  
  // private, use accessors
  private lazy val category: TextView = R.id.category
  private lazy val check: CheckBox = android.R.id.checkbox

  def getCategory = {
    category.setMovementMethod(LinkMovementMethod.getInstance)
    category
  }

  def getCheck = {
    check.setVisibility(View.VISIBLE)
    check
  }

  def setClickable(enabled: Boolean) = base.setClickable(enabled)

  // whether the ad has an image or not
  def showPhoto(enabled: Boolean) = enabled match {
    // show photo
    case true =>
      image.setVisibility(View.GONE)
      frame.setVisibility(View.VISIBLE)
      
    // hide photo
    case false =>
      frame.setVisibility(View.GONE)
      image.setVisibility(View.GONE)
  }
}