package com.vittlepix.widget
import android.view.View

/**
 * trait for wrapped row list views
 */
trait Wrappable {

  protected val _base: View
  
  implicit def int2View[A <: View](id: Int): A = _base.findViewById(id).asInstanceOf[A]
}